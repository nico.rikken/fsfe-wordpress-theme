<?php
// SPDX-FileCopyrightText: 2019-2022 WordPress Theme Experiments contributors
//
// SPDX-License-Identifier: GPL-2.0-or-later

if ( ! function_exists( 'fsfe_blogs_support' ) ) :
	function fsfe_blogs_support()  {

		// Adding support for core block visual styles.
		add_theme_support( 'wp-block-styles' );

		// Enqueue editor styles.
		add_editor_style( 'style.css' );

		// Enable custom backgrounds.
		// Documentation: https://developer.wordpress.org/themes/functionality/custom-backgrounds/
		$args = array(
			'default-color' => '0000ff',
			// TODO: integrate images in theme like: 'default-image' => get_template_directory_uri() . '/images/wapuu.jpg',
			'default-image' => 'https://fsfe.org/graphics/fellowship/plussy_tile_03.svg'
		);
		add_theme_support( 'custom-background', $args );
	}
	add_action( 'after_setup_theme', 'fsfe_blogs_support' );
endif;

/**
 * Enqueue scripts and styles.
 */
function fsfe_blogs_scripts() {
	// Enqueue theme stylesheet.
	wp_enqueue_style( 'fsfe-blogs-style', get_template_directory_uri() . '/style.css', array(), wp_get_theme()->get( 'Version' ) );
}

add_action( 'wp_enqueue_scripts', 'fsfe_blogs_scripts' );
