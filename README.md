<!--
SPDX-FileCopyrightText: 2022 Nico Rikken <nico.rikken@fsfe.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# FSFE WordPress theme

This dedicated WordPress theme is supposed to become the default theme for the blogging platform offered to supporters
More information about the blogging platform is available on the [TechDocs/Blogs wiki page](https://wiki.fsfe.org/TechDocs/Blogs).

*This project is still in early development.*

## Development environment

A basic Docker Compose configuration for running WordPress with a MySQL database is included in the repository.
Docker has provided this configuration as a [documented example of using Docker Compose](https://docs.docker.com/samples/wordpress/).

There is official documentation by Docker on [how to install Docker Compose](https://docs.docker.com/compose/install/).

On Debian you should be able to get a working setup with the following commands:

```shell
sudo apt install docker.io docker-compose
sudo docker-compose up
```

If you prefer Podman for running containers you can [let Docker Compose use Podman as the backend](https://www.redhat.com/sysadmin/podman-docker-compose).
When using Podman together with Docker Compose on Debian, the basic setup can be obtained by running:

```shell
sudo apt install podman docker-compose
sudo systemctl start podman.socket
sudo docker-compose up
```

Now WordPress should be available on [http://localhost:8000](http://localhost:8000)

Before you can use it, you should walk through the basic setup wizard to name your blog, choose and administrator password and provide an email address.

### How it works

The theme in `fsfe-blogs` directory is mounted in the `wordpress` container at `/var/www/html/wp-content/themes/fsfe-blogs-dev`.
This mount works in read-write mode so if you want you can also work on the theme from inside the WordPress container.
If you make changes from inside the WordPress container, be aware that the file ownership might change, resulting in inaccessible files.

### Pre-commit

To help maintain the quality of this codebase git commit hooks are provided to check and format files.
The commit hooks in this project are provided by the community through the [pre-commit](https://pre-commit.com/) project.
Pre-commit is available for installation in many places including distribution repositories and Pip.

Run `pre-commit install` to set up the git hook scripts.

Optionally run the hooks against all files before `git commit` using the `pre-commit run --all-files` command.

### Compiling style.css

The WordPress theme style is defined in style.css.
Similar to the fsfe.org website the actual CSS is built up from the styles in the `look/` directory.
In fact because of the shared hertigage it should be able to keep these files synchronized.
[Less](https://lesscss.org) is used as the source to compile the source styles into the final CSS file.

Due to some issues in the underlying Bootstrap files, the stylesheet can not be compiled using the newer version `4.x.x` release.
Version `3.13.1` has to be used at time of writing.
If you want you can install Less globally on your system, but this is not needed.
Less is included in the `package.json` including a convenient wrapper based on [onchange](https://www.npmjs.com/package/onchange):

```shell
npm install
npm run build:css # build once
npm run watch:css # watch for changes and build if needed
```

**Important!** For completeness and ease of use the rendered `style.css` file should be committed to git.

## Useful resources

### WordPress theming

- [WordPress Block Editor handbook](https://developer.wordpress.org/block-editor/) explaining the new Block themes.
- [WordPress theme-experiments repository](https://github.com/WordPress/theme-experiments/) containing Block theme experiments and a script to create a new one.
- [WordPress Theme Handbook](https://developer.wordpress.org/themes/) covering many aspects of developing WordPress themes.
- [WordPress Twenty Twenty-Two theme](https://github.com/WordPress/twentytwentytwo) that focuses on WordPress features instead of heavy CSS styling.
- [Latest theme.json JSON-schema](https://raw.githubusercontent.com/WordPress/gutenberg/trunk/schemas/json/theme.json) that shows all available styling options.

### FSFE style and reference material

- [FSFE Style Guide](https://fsfe.org/contribute/designers/styleguide.html)
- [FSFE website look directory](https://git.fsfe.org/FSFE/fsfe-website/src/branch/master/look/) containing the source of the visual style of the fsfe.org website.
- [FSFE wiki](https://wiki.fsfe.org/) in accordance with the FSFE style.
- [FSFE planet](https://planet.fsfe.org/) displaying blogs of FSFE supporters.

## License

This work is licensed under multiple licences.

 - All original source code is licensed under GPL-2.0-or-later.
 - All documentation is licensed under CC-BY-SA-4.0.
 - Some configuration and data files are licensed under CC0-1.0.

For more accurate information, check the individual files.

The [license of the WordPress software](https://wordpress.org/about/license/) was considered in choosing these licenses.
